class CountriesController < ApplicationController
  soap_service namespace: 'urn:WashOut'

  soap_action "soap_country", :return => :string

  def soap_country
    render :soap => country_with_money
  end

private
  def country_with_money
    countries = Country.all
    countries.map do |country|
      next unless Country[country.last].currency
      money = Country[country.last].currency
      {
        country_name: country.first,
        country_code: country.last,
        money_code: money.code,
        money_name: money.name,
        money_symbol: money.symbol
      }
    end.compact
  end
end
